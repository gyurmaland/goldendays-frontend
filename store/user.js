export const state = () => ({
  connectedWithFb: false,
  alreadyVoted: false
});

export const mutations = {
  setAlreadyVoted(state, alreadyVoted) {
    state.alreadyVoted = alreadyVoted
  }
};

export const actions = {};

export const getters = {
  alreadyVoted(state) {
    return state.alreadyVoted
  },
};
