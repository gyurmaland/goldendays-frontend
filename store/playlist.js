export const state = () => ({
  selectedTracks: [],
});

export const mutations = {
  setSelectedTracks(state, selectedTracks) {
    state.selectedTracks = selectedTracks
  },
  addSelectedTrack(state, selectedTrack) {
    state.selectedTracks.push(selectedTrack)
  },
  removeSelectedTrack(state, selectedTrack) {
    state.selectedTracks.splice(state.selectedTracks.indexOf(selectedTrack), 1)
  },
};

export const actions = {};

export const getters = {
  selectedTracks(state) {
    return state.selectedTracks
  },
};
