export const state = () => ({
  user: {
    id: '',
    accessToken: '',
    email: '',
    name: '',
    picture: {
      data: {
        url: ''
      }
    }
  }
});

export const mutations = {
  setUser(state, user) {
    state.user = user
  },
  setUserPic(state, pic) {
    state.user.picture = pic
  },
  setAccessToken(state, accessToken) {
    state.user.accessToken = accessToken
  },
};

export const actions = {};

export const getters = {
  user(state) {
    return state.user
  },
};
