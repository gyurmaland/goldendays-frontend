import axios from 'axios'

export default() => {
  return axios.create({
    baseURL: process.env.NODE_ENV !== 'production' ? 'http://localhost:8080/' : 'https://goldendays.herokuapp.com/',
    withCredentials: false,
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    }
  })
}
